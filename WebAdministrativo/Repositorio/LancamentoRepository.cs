﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class LancamentoRepository
    {
        private AdministrativoContext context = new AdministrativoContext();

        public List<Lancamento> BuscaTodas()
        {
            var consulta = from e in context.Lancamento
                           select e;
            return consulta.ToList();
        }

        public void Adiciona(Lancamento e)
        {
            context.Lancamento.Add(e);
            context.SaveChanges();
        }
    }
}