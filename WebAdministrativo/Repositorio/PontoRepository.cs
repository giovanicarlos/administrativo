﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class PontoRepository : Repository<Ponto>
    {
        private AdministrativoContext context = new AdministrativoContext();

        public IQueryable<Ponto> PontosMesAtual()
        {
            DateTime inicioMes = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime fimMes = DateTime.Now.AddMonths(1);
            fimMes = new DateTime(fimMes.Year, fimMes.Month, fimMes.Day, 23, 59, 59);
            fimMes = fimMes.AddDays(-1);
            return dbset.Where(x => x.Dia >= inicioMes && x.Dia <= fimMes).OrderBy(o => o.Dia);
        }
    }
}