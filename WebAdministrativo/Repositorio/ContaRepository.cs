﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class ContaRepository
    {
        private AdministrativoContext context = new AdministrativoContext();

        public List<Conta> BuscaTodas()
        {
            var consulta = from e in context.Conta
                           select e;
            return consulta.ToList();
        }
    }
}