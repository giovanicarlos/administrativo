﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;
using System.Data;

namespace WebAdministrativo.Repositorio
{
    public class PessoaFisicaRepository : Repository<PessoaFisica>
    {
        private AdministrativoContext context = new AdministrativoContext();

        public void Adiciona(PessoaFisica pf)
        {
            context.PessoaFisica.Add(pf);
            context.SaveChanges();
        }

        public void Altera(PessoaFisica pf)
        {
            context.Entry(pf).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}