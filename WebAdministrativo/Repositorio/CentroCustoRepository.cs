﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class CentroCustoRepository : Repository<CentroCusto>
    {
        private AdministrativoContext context = new AdministrativoContext();
        public List<CentroCusto> BuscaTodas2()
        {
            //var consulta = from e in context.CentroCusto
            //               select e;
            //var consulta = from e in context.CentroCusto where e.CentroCustoPaiId == null select e;

            //foreach (CentroCusto cc in consulta)
            //{
            //    this.ge
            //}
            //return consulta.ToList();


            var list = from e in context.CentroCusto where e.CentroCustoPaiId == null select e;
            foreach (CentroCusto cc in list)
            {
                //list.AddRange((from x in context.CentroCusto where x.CentroCustoPai.CentroCustoId == cc.CentroCustoId select x).ToList());
                list.Union(Consultar(cc.CentroCustoId));
            }
            return list.ToList();
        }

        public IQueryable<CentroCusto> Consultar(int? idPai)
        {
            if (!idPai.Equals(null))
                return dbset.Where(x => x.CentroCustoPaiId == idPai).OrderBy(x => x.Nome);
            else
                return dbset.Where(x => x.CentroCustoPaiId == null).OrderBy(x => x.Nome);
            
        }

        public IQueryable<CentroCusto> Todos()
        {
            List<CentroCusto> CentrosDeCusto = Consultar(null).ToList();
            List<CentroCusto> CentrosDeCustoF = new List<CentroCusto>();
            foreach (CentroCusto cc in CentrosDeCusto)
            {
                CentrosDeCustoF.Add(cc);
                CentrosDeCustoF.AddRange(Consultar(cc.CentroCustoId).ToList());
            }

            return CentrosDeCustoF.AsQueryable();
        }

    }
}