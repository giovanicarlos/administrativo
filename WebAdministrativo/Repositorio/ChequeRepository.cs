﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;
using System.Data;

namespace WebAdministrativo.Repositorio
{
    public class ChequeRepository : Repository<Cheque>
    {
        private AdministrativoContext context = new AdministrativoContext();
        public IQueryable<Cheque> Todos()
        {
            return dbset.OrderByDescending(x => x.ChequeId);
        }

        public void Adiciona(Cheque c)
        {
            context.Cheque.Add(c);
            context.SaveChanges();
        }

        public void Altera(Cheque c)
        {
            context.Entry(c).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}