﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Repositorio.Interfaces;
using System.Data.Entity;
using System.Data;
using System.Linq.Expressions;

namespace WebAdministrativo.Repositorio
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbSet<T> dbset;
        private DbContext context;

        public Repository()
        {
            context = new AdministrativoContext();
            dbset = context.Set<T>();
        }

        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual T GetById(int id)
        {
            return dbset.Find(id);
        }

        public virtual IQueryable<T> GetAll()
        {
            return dbset;
        }

        public virtual IQueryable<T> GetAll(Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            totalPages = dbset.Count();
            return dbset.OrderBy(order).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();
        }

        public virtual IQueryable<T> GetMany(Expression<Func<T, bool>> where, Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            totalPages = dbset.Where(where).Count();
            return dbset.Where(where).OrderBy(order).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();
        }

        public virtual void Commit()
        {
            context.SaveChanges();
        }

        public virtual IQueryable<T> Recursivo(Func<T, IEnumerable<T>> before, Func<T, IEnumerable<T>> after)
        {
            return dbset.Flatten(before, after).AsQueryable();
        }

        //public virtual IEnumerable<T> Flatten<T>(this IEnumerable<T> items, Func<T, IEnumerable<T>> before, Func<T, IEnumerable<T>> after)
        //{
        //    foreach (var item in items)
        //    {
        //        if (before != null)
        //        {
        //            foreach (var b in before(item))
        //            {
        //                yield return b;
        //            }
        //        }
        //        yield return item;
        //        if (after != null)
        //        {
        //            foreach (var a in after(item))
        //            {
        //                yield return a;
        //            }
        //        }
        //    }
        //}
    }
}