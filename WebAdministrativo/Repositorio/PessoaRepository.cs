﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class PessoaRepository : Repository<Pessoa>
    {
        private AdministrativoContext context = new AdministrativoContext();
        public IQueryable<Pessoa> Todos()
        {
            return dbset.OrderBy(x => x.Nome);
        }


        public Pessoa Validar(string email, string senha)
        {
            var consulta = (from e in context.Pessoa
                            where (e.Email.Equals(email) && e.Senha.Equals(senha))
                            select e);
            return consulta.FirstOrDefault();
        }
    }
}