﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class AdministrativoContext : DbContext
    {
        public DbSet<DividaFutura> DividaFutura { get; set; }
        public DbSet<CentroCusto> CentroCusto { get; set; }
        public DbSet<Conta> Conta { get; set; }
        public DbSet<Cheque> Cheque { get; set; }
        public DbSet<Pessoa> Pessoa { get; set; }
        public DbSet<PessoaFisica> PessoaFisica { get; set; }
        public DbSet<PessoaJuridica> PessoaJuridica { get; set; }
        public DbSet<Administrador> Administrador { get; set; }
        public DbSet<Lancamento> Lancamento { get; set; }
        public DbSet<Ponto> Ponto { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<DividaFutura>().ToTable("tblDividaFutura");
            modelBuilder.Entity<CentroCusto>().ToTable("tblCentroCusto");
            modelBuilder.Entity<Conta>().ToTable("tblConta");
            modelBuilder.Entity<Cheque>().ToTable("tblCheque");
            modelBuilder.Entity<Pessoa>().ToTable("tblPessoa");
            modelBuilder.Entity<PessoaFisica>().ToTable("tblPessoaFisica");
            modelBuilder.Entity<PessoaJuridica>().ToTable("tblPessoaJuridica");
            modelBuilder.Entity<Administrador>().ToTable("tblAdministrador");
            modelBuilder.Entity<Lancamento>().ToTable("tblLancamento");
            modelBuilder.Entity<Ponto>().ToTable("tblPonto");

            modelBuilder.Entity<DividaFutura>().Property(x => x.Nome).IsRequired().HasMaxLength(150);
            modelBuilder.Entity<CentroCusto>().Property(x => x.Nome).IsRequired().HasMaxLength(150);
            modelBuilder.Entity<Conta>().Property(x => x.Banco).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Cheque>().Property(x => x.Numero).IsRequired().HasMaxLength(20);
            modelBuilder.Entity<Cheque>().Property(x => x.Descricao).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Pessoa>().Property(x => x.Nome).IsRequired().HasMaxLength(150);
            modelBuilder.Entity<CentroCusto>().HasOptional(e => e.CentroCustoPai).WithMany().HasForeignKey(m => m.CentroCustoPaiId); //Auto relacionamento
            //modelBuilder.Entity<Administrador>().Property(x => x.Email).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }

}