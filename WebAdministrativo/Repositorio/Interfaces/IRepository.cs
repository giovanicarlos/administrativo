﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Repositorio.Interfaces
{
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Adiciona uma nova entidade ao repositório de dados
        /// </summary>
        /// <param name="entity">Entidade a ser adicionada</param>
        void Add(T entity);

        /// <summary>
        /// Atualiza uma entidade no repositório de dados
        /// </summary>
        /// <param name="entity">Entidade a ser atualizada</param>
        void Update(T entity);

        /// <summary>
        /// Deleta uma entidade do repositório de dados
        /// </summary>
        /// <param name="entity">Entidade a ser deletada</param>
        void Delete(T entity);

        T GetById(int id);

        /// <summary>
        /// Obtém um IQueryable de todas as entidades do repositório
        /// </summary>
        /// <returns>Um objeto IQueryable tipado para a entidade</returns>
        IQueryable<T> GetAll();

        void Commit();
    }
}