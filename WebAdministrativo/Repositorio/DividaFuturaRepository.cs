﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Repositorio
{
    public class DividaFuturaRepository
    {
        private AdministrativoContext context = new AdministrativoContext();
        public List<DividaFutura> BuscaTodas()
        {
            var consulta = from e in context.DividaFutura
                           select e;
            return consulta.ToList();
        }

        public void Adiciona(DividaFutura e)
        {
            context.DividaFutura.Add(e);
            context.SaveChanges();
        }
    }
}