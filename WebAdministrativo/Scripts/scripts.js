﻿function getDate() {
    var fullDate = new Date();
    var twoDigitMonth = fullDate.getMonth() + 1;
    var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
    return currentDate;
}