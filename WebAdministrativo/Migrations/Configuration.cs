namespace WebAdministrativo.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebAdministrativo.Dominio;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<WebAdministrativo.Repositorio.AdministrativoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private WebAdministrativo.Repositorio.AdministrativoContext _context;

        protected override void Seed(WebAdministrativo.Repositorio.AdministrativoContext context)
        {
            _context = context;

            CentroCusto ccAcerto = new CentroCusto { Nome = "Ajuste de saldo" };
            CentroCusto ccEducacao = new CentroCusto { Nome = "Educa��o" };
            CentroCusto ccDespesas = new CentroCusto { Nome = "Casa" };
            CentroCusto ccCarro = new CentroCusto { Nome = "Carro" };
            CentroCusto ccLazer = new CentroCusto { Nome = "Lazer" };
            CentroCusto ccSaude = new CentroCusto { Nome = "Sa�de" };

            List<CentroCusto> custosIniciais = new List<CentroCusto>();
            custosIniciais.Add(ccAcerto);
            custosIniciais.Add(new CentroCusto { Nome = "Bens" });
            custosIniciais.Add(ccDespesas);
            custosIniciais.Add(new CentroCusto { Nome = "�gua", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Luz", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Telefone", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "G�s", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Supermercado", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Padaria", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Internet", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "IPTU", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Almo�o", CentroCustoPai = ccDespesas });
            custosIniciais.Add(new CentroCusto { Nome = "Presta��o da Casa", CentroCustoPai = ccDespesas });
            custosIniciais.Add(ccEducacao);
            custosIniciais.Add(new CentroCusto { Nome = "P�s Gradua��o Giovani", CentroCustoPai = ccEducacao });
            custosIniciais.Add(new CentroCusto { Nome = "P�s Gradua��o Liliana", CentroCustoPai = ccEducacao });
            custosIniciais.Add(new CentroCusto { Nome = "Escola Laura", CentroCustoPai = ccEducacao });
            custosIniciais.Add(ccCarro);
            custosIniciais.Add(new CentroCusto { Nome = "Revis�o", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "Conserto", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "Gasolina", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "Troca de �leo", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "Seguro", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "IPVA", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "DPVAT", CentroCustoPai = ccCarro });
            custosIniciais.Add(new CentroCusto { Nome = "Taxa de Licenciamento", CentroCustoPai = ccCarro });
            custosIniciais.Add(ccLazer);
            custosIniciais.Add(new CentroCusto { Nome = "Praia", CentroCustoPai = ccLazer });
            custosIniciais.Add(new CentroCusto { Nome = "Carnalfenas", CentroCustoPai = ccLazer });
            custosIniciais.Add(new CentroCusto { Nome = "Viagem", CentroCustoPai = ccLazer });
            custosIniciais.Add(new CentroCusto { Nome = "Anivers�rio", CentroCustoPai = ccLazer });
            custosIniciais.Add(ccSaude);
            custosIniciais.Add(new CentroCusto { Nome = "Plano de Sa�de", CentroCustoPai = ccSaude });
            custosIniciais.Add(new CentroCusto { Nome = "Farm�cia", CentroCustoPai = ccSaude });
            custosIniciais.Add(new CentroCusto { Nome = "Futebol", CentroCustoPai = ccSaude });
            custosIniciais.Add(new CentroCusto { Nome = "Hidrogin�stica", CentroCustoPai = ccSaude });

            Seed<CentroCusto>(custosIniciais);

            List<Conta> contas = new List<Conta>();
            contas.Add(new Conta { Banco = "Santander", Status = true });
            contas.Add(new Conta { Banco = "Caixa Econ�mica Federal", Status = true });
            contas.Add(new Conta { Banco = "Banco do Brasil", Status = true });
            Seed<Conta>(contas);

            List<PessoaFisica> pessoas = new List<PessoaFisica>();
            pessoas.Add(new PessoaFisica { Nome = "Laura da Silva Andrade", Endereco = "Rua Umbelina da Silveira Pinto, 432", enmSexo = enumSexo.Feminino, Senha = "123", Status = true });
            Seed<PessoaFisica>(pessoas);

            List<Administrador> admins = new List<Administrador>();
            admins.Add(new Administrador { Nome = "Giovani Carlos de Andrade", Email = "giovanicarlos@gmail.com", Endereco = "Rua Umbelina da Silveira Pinto, 432", enmSexo = enumSexo.Masculino, Senha = "123".ToCriptografa(), Status = true });
            admins.Add(new Administrador { Nome = "Liliana Aparecida da Silva", Email = "liliana.andrade@yahoo.com.br", Endereco = "Rua Umbelina da Silveira Pinto, 432", enmSexo = enumSexo.Feminino, Senha = "123".ToCriptografa(), Status = true });
            Seed<Administrador>(admins);

            Lancamento lanc = new Lancamento { CentroCusto = custosIniciais[0], Conta = contas[0], Data = DateTime.Now, Descricao = "Inicio", enmFormaPagamento = enumFormaPagamento.CartaoUnicardVisa, enmOperacao = enumOperacao.Credito, Valor = 1 };
            Seed<Lancamento>(lanc);

            Cheque cheque = new Cheque { Data = DateTime.Now, DataCompensacao = DateTime.Now, Descricao = "Primeiro cheque", Numero = "1", Pago = false, Valor = 100 };
            Seed<Cheque>(cheque);

            base.Seed(context);
        }

        private void Seed<T>(T entity) where T : class
        {
            _context.Set<T>().Add(entity);
        }

        private void Seed<T>(List<T> entities) where T : class
        {
            foreach (T t in entities)
                _context.Set<T>().Add(t);
        }

    }
}
