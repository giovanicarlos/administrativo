namespace WebAdministrativo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tblDividaFutura",
                c => new
                    {
                        DividaFuturaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Prestacoes = c.Int(nullable: false),
                        Termino = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DividaFuturaId);
            
            CreateTable(
                "dbo.tblCentroCusto",
                c => new
                    {
                        CentroCustoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150),
                        CentroCustoPaiId = c.Int(),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CentroCustoId)
                .ForeignKey("dbo.tblCentroCusto", t => t.CentroCustoPaiId)
                .Index(t => t.CentroCustoPaiId);
            
            CreateTable(
                "dbo.tblConta",
                c => new
                    {
                        ContaId = c.Int(nullable: false, identity: true),
                        Banco = c.String(nullable: false, maxLength: 100),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ContaId);
            
            CreateTable(
                "dbo.tblCheque",
                c => new
                    {
                        ChequeId = c.Int(nullable: false, identity: true),
                        Numero = c.String(nullable: false, maxLength: 20),
                        Descricao = c.String(nullable: false, maxLength: 50),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Data = c.DateTime(nullable: false),
                        DataCompensacao = c.DateTime(nullable: false),
                        Pago = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ChequeId);
            
            CreateTable(
                "dbo.tblPessoa",
                c => new
                    {
                        PessoaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150),
                        Email = c.String(),
                        Senha = c.String(),
                        Endereco = c.String(),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PessoaId);
            
            CreateTable(
                "dbo.tblLancamento",
                c => new
                    {
                        LancamentoId = c.Int(nullable: false, identity: true),
                        Data = c.DateTime(nullable: false),
                        Descricao = c.String(),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Pago = c.Boolean(nullable: false),
                        CentroCustoId = c.Int(nullable: false),
                        ContaId = c.Int(nullable: false),
                        ChequeId = c.Int(),
                        PessoaId = c.Int(),
                        Operacao = c.Byte(nullable: false),
                        FormaPagamento = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.LancamentoId)
                .ForeignKey("dbo.tblCentroCusto", t => t.CentroCustoId)
                .ForeignKey("dbo.tblConta", t => t.ContaId)
                .ForeignKey("dbo.tblCheque", t => t.ChequeId)
                .ForeignKey("dbo.tblPessoa", t => t.PessoaId)
                .Index(t => t.CentroCustoId)
                .Index(t => t.ContaId)
                .Index(t => t.ChequeId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.tblPonto",
                c => new
                    {
                        PontoId = c.Int(nullable: false, identity: true),
                        Dia = c.DateTime(nullable: false),
                        Entrada = c.DateTime(),
                        Saida = c.DateTime(),
                        Observacao = c.String(),
                        DataCadastro = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PontoId);
            
            CreateTable(
                "dbo.tblPessoaFisica",
                c => new
                    {
                        PessoaId = c.Int(nullable: false),
                        Nascimento = c.DateTime(),
                        Sexo = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.PessoaId)
                .ForeignKey("dbo.tblPessoa", t => t.PessoaId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.tblPessoaJuridica",
                c => new
                    {
                        PessoaId = c.Int(nullable: false),
                        Contato = c.String(),
                    })
                .PrimaryKey(t => t.PessoaId)
                .ForeignKey("dbo.tblPessoa", t => t.PessoaId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.tblAdministrador",
                c => new
                    {
                        PessoaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PessoaId)
                .ForeignKey("dbo.tblPessoaFisica", t => t.PessoaId)
                .Index(t => t.PessoaId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.tblAdministrador", new[] { "PessoaId" });
            DropIndex("dbo.tblPessoaJuridica", new[] { "PessoaId" });
            DropIndex("dbo.tblPessoaFisica", new[] { "PessoaId" });
            DropIndex("dbo.tblLancamento", new[] { "PessoaId" });
            DropIndex("dbo.tblLancamento", new[] { "ChequeId" });
            DropIndex("dbo.tblLancamento", new[] { "ContaId" });
            DropIndex("dbo.tblLancamento", new[] { "CentroCustoId" });
            DropIndex("dbo.tblCentroCusto", new[] { "CentroCustoPaiId" });
            DropForeignKey("dbo.tblAdministrador", "PessoaId", "dbo.tblPessoaFisica");
            DropForeignKey("dbo.tblPessoaJuridica", "PessoaId", "dbo.tblPessoa");
            DropForeignKey("dbo.tblPessoaFisica", "PessoaId", "dbo.tblPessoa");
            DropForeignKey("dbo.tblLancamento", "PessoaId", "dbo.tblPessoa");
            DropForeignKey("dbo.tblLancamento", "ChequeId", "dbo.tblCheque");
            DropForeignKey("dbo.tblLancamento", "ContaId", "dbo.tblConta");
            DropForeignKey("dbo.tblLancamento", "CentroCustoId", "dbo.tblCentroCusto");
            DropForeignKey("dbo.tblCentroCusto", "CentroCustoPaiId", "dbo.tblCentroCusto");
            DropTable("dbo.tblAdministrador");
            DropTable("dbo.tblPessoaJuridica");
            DropTable("dbo.tblPessoaFisica");
            DropTable("dbo.tblPonto");
            DropTable("dbo.tblLancamento");
            DropTable("dbo.tblPessoa");
            DropTable("dbo.tblCheque");
            DropTable("dbo.tblConta");
            DropTable("dbo.tblCentroCusto");
            DropTable("dbo.tblDividaFutura");
        }
    }
}
