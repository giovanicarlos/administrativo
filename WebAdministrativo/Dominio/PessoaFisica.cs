﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public class PessoaFisica : Pessoa
    {
        public DateTime? Nascimento { get; set; }
        public byte Sexo { get; set; }

        public enumSexo enmSexo
        {
            get { return (enumSexo)Sexo; }
            set { Sexo = (byte)value; }
        }
    }

    public enum enumSexo
    {
        Masculino = 1,
        Feminino = 0
    }

}