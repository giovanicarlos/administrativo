﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public class Ponto
    {
        public int PontoId { get; set; }
        public DateTime Dia { get; set; }
        public DateTime? Entrada { get; set; }
        public DateTime? Saida { get; set; }
        public string Observacao { get; set; }
        public DateTime DataCadastro { get; set; }

        public Ponto()
        {
            this.DataCadastro = DateTime.Now;
        }

        public Ponto(DateTime dia, DateTime? entrada, DateTime? saida, string observacao)
            : this()
        {
            this.Dia = dia;
            this.Entrada = entrada;
            this.Saida = saida;
            this.Observacao = observacao;
        }

        public void Alterar(DateTime dia, DateTime? entrada, DateTime? saida, string observacao)
        {
            this.Dia = dia;
            this.Entrada = entrada;
            this.Saida = saida;
            this.Observacao = observacao;
        }
    }
}