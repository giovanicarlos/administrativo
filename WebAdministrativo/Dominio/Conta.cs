﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public class Conta
    {
        public int ContaId { get; set; }
        public string Banco { get; set; }
        public bool Status { get; set; }
    }
}