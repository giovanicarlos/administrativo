﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAdministrativo.Dominio
{
    public class Cheque
    {
        public int ChequeId { get; set; }
        
        [Display(Name="Número")]
        [Required(ErrorMessage = "Digite o número do cheque.")]
        public string Numero { get; set; }
        
        [Display(Name = "Destinatário")]
        [Required(ErrorMessage = "Informe o destinatário do cheque.")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Digite o valor do cheque.")]
        [DataType(DataType.Currency, ErrorMessage="Formato inválido.")]
        public decimal Valor { get; set; }

        [Display(Name = "Data de emissão")]
        [Required(ErrorMessage = "Informe a data de emissão do cheque.")]
        public DateTime Data{ get; set; }

        [Display(Name = "Data de compensação")]
        [Required(ErrorMessage = "Informe a data prevista para pagamento do cheque.")]
        public DateTime DataCompensacao { get; set; }
        
        [Display(Name = "Compensado")]
        public bool Pago { get; set; }
    }
}