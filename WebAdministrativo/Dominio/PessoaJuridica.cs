﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public class PessoaJuridica : Pessoa
    {
        public string Contato { get; set; }
    }
}