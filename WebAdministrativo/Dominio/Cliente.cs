﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public class Cliente
    {
        public Cliente(){ }
        public Cliente(object obj)
        {
            if (obj is Pessoa)
            {
                Pessoa p = obj as Pessoa;
                this.PessoaId = p.PessoaId;
                this.Nome = p.Nome;
                this.Email = p.Email;
                this.Senha = p.Senha;
                this.Endereco = p.Endereco;
                this.Status = p.Status;
            }

            if(obj is PessoaFisica)
            {
                PessoaFisica pFisica = obj as PessoaFisica;
                this.Nascimento = pFisica.Nascimento;
                this.Sexo = pFisica.Sexo;
                this.TipoPessoa = (byte)enumTipoPessoa.Fisica;
            }
            else if(obj is PessoaJuridica)
            {
                PessoaJuridica pJuridica = obj as PessoaJuridica;
                this.Contato = pJuridica.Contato;
                this.TipoPessoa = (byte)enumTipoPessoa.Juridica;
            }
        }
        //Pessoa
        public int PessoaId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Endereco { get; set; }
        public bool Status { get; set; }
        public byte TipoPessoa { get; set; }

        public enumTipoPessoa enmTipoPessoa
        {
            get { return (enumTipoPessoa)TipoPessoa; }
            set { TipoPessoa = (byte)value; }
        }

        //PessoaFisica
        public DateTime? Nascimento { get; set; }
        public byte Sexo { get; set; }

        public enumSexo enmSexo
        {
            get { return (enumSexo)Sexo; }
            set { Sexo = (byte)value; }
        }

        //PessoaJuridica
        public string Contato { get; set; }
    }

    public enum enumTipoPessoa
    {
        Fisica = 1,
        Juridica = 2
    }
}