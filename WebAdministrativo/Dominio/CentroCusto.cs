﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAdministrativo.Dominio
{
    public class CentroCusto
    {
        public CentroCusto()
        {
            Status = true;
        }

        public int CentroCustoId { get; set; }
        public string Nome { get; set; }

        public int? CentroCustoPaiId { get; set; }
        public virtual CentroCusto CentroCustoPai { get; set; }

        public bool Status { get; set; }
    }
}