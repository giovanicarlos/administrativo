﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public class DividaFutura
    {
        public int DividaFuturaId { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public int Prestacoes { get; set; }
        public DateTime Termino { get; set; }
    }
}