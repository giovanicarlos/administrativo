﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAdministrativo.Dominio
{
    public class Lancamento
    {
        public Lancamento()
        {
            this.enmOperacao = enumOperacao.Debito;
        }

        public int LancamentoId { get; set; }
        
        [Required(ErrorMessage = "Informe a data prevista para pagamento deste lançamento.")]
        public DateTime Data { get; set; }
        
        public string Descricao { get; set; }
        
        [Required(ErrorMessage = "Digite o valor deste lançamento.")]
        public decimal Valor { get; set; }
        public bool Pago { get; set; }

        [Required(ErrorMessage = "Selecione um centro de custo.")]
        public int CentroCustoId { get; set; }
        public virtual CentroCusto CentroCusto { get; set; }

        public int ContaId { get; set; }
        public virtual Conta Conta { get; set; }

        public int? ChequeId { get; set; }
        public virtual Cheque Cheque { get; set; }

        public int? PessoaId { get; set; }
        public virtual Pessoa Cedente { get; set; }

        public byte Operacao { get; set; }
        public enumOperacao enmOperacao
        {
            get { return (enumOperacao)Operacao; }
            set { Operacao = (byte)value; }
        }

        public byte FormaPagamento { get; set; }
        public enumFormaPagamento enmFormaPagamento 
        {
            get { return (enumFormaPagamento)FormaPagamento; }
            set { FormaPagamento = (byte)value; }
        }

    }

    public enum enumOperacao
    {
        Debito = 0,
        Credito = 1
    }

    public enum enumFormaPagamento
    {
        Dinheiro = 1,
        ChequeSantander = 2,
        CartaoUnicardVisa = 3,
        CartaoCreditoSantanderGold = 4,
        CartaoDebitoSantanderGold = 5,
        CartaoCreditoSubmarino = 6,
        CartaoCreditoPetrobrás = 7
    }
}