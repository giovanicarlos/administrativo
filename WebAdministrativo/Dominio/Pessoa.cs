﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Dominio
{
    public abstract class Pessoa
    {
        public int PessoaId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Endereco { get; set; }
        public bool Status { get; set; }
    }
}