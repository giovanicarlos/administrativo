﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAdministrativo.Servico.Interfaces;
using WebAdministrativo.Repositorio.Interfaces;
using WebAdministrativo.Repositorio;

namespace WebAdministrativo.Servico
{
    public class BaseServico<T> : IBaseServico<T> where T : class
    {
        private IRepository<T> baseRepositorio;

        public BaseServico()
        {
            this.baseRepositorio = new Repository<T>();
        }

        public void Add(T entity)
        {
            baseRepositorio.Add(entity);
            baseRepositorio.Commit();
        }

        public void Update(T entity)
        {
            baseRepositorio.Update(entity);
            baseRepositorio.Commit();
        }

        public void Delete(T entity)
        {
            baseRepositorio.Delete(entity);
            baseRepositorio.Commit();
        }

        public T GetById(int Id)
        {
            return baseRepositorio.GetById(Id);
        }
    }
}