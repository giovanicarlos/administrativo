﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using WebAdministrativo.Dominio;
using WebAdministrativo.Repositorio;

namespace WebAdministrativo.Servico
{
    public class PontoServico : BaseServico<Ponto>
    {
        private PontoRepository pontoRepository;

        public PontoServico()
        {
            this.pontoRepository = new PontoRepository();
        }

        public IEnumerable<Ponto> PontosMesAtual()
        {
            return pontoRepository.PontosMesAtual();
        }
    }
}