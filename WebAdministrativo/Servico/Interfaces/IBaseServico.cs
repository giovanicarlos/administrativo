﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAdministrativo.Servico.Interfaces
{
    public interface IBaseServico<T> where T : class
    {
        void Add(T entity);
    }
}
