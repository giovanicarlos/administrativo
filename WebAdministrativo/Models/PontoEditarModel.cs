﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAdministrativo.Models
{
    public class PontoEditarModel
    {
        public int? PontoId { get; set; }
        [Required(ErrorMessage = "Informe o dia do ponto.")]
        public DateTime Dia { get; set; }
        public DateTime? Entrada { get; set; }
        public DateTime? Saida { get; set; }
        public string Observacao { get; set; }
        public DateTime DataCadastro { get; set; }

        public PontoEditarModel(){ }
        public PontoEditarModel(int? idPonto, DateTime dia, DateTime? entrada, DateTime? saida, string observacao, DateTime dataCadastro)
        {
            this.PontoId = idPonto;
            this.Dia = dia;
            this.Entrada = entrada;
            this.Saida = saida;
            this.Observacao = observacao;
            this.DataCadastro = dataCadastro;
        }
    }
}