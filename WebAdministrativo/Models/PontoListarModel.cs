﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAdministrativo.Models
{
    public class PontoListarModel
    {
        public DateTime Dia { get; set; }
        public ICollection<PontoEntradaSaidaModel> Turnos;
        public bool Completo 
        {
            get
            {
                if (Turnos.Count >= 2)
                {
                    bool datasPreenchidas = true;
                    foreach (PontoEntradaSaidaModel registro in Turnos)
                    {
                        if (!registro.Entrada.HasValue || !registro.Saida.HasValue)
                            datasPreenchidas = false;
                    }
                    return datasPreenchidas;
                }
                return false;
            }
        }

        public TimeSpan? TotalDoDia 
        {
            get 
            {
                if (Turnos.Count > 0)
                {
                    TimeSpan horasTrabalhadas = new TimeSpan();
                    foreach (PontoEntradaSaidaModel registro in Turnos)
                    {
                        if (registro.Entrada.HasValue && registro.Saida.HasValue)
                        {
                            TimeSpan diferenca = registro.Saida.Value.Subtract(registro.Entrada.Value);
                            horasTrabalhadas = horasTrabalhadas.Add(diferenca);
                        }
                    }
                    return horasTrabalhadas;
                }
                return null;
            }
        }

        public TimeSpan? SaldoDoDia
        {
            get 
            { 
                if(TotalDoDia.HasValue)
                {
                    TimeSpan total = new TimeSpan(8, 0, 0);
                    TimeSpan? result = TotalDoDia.Value.Subtract(total);
                    return result;
                }
                return null;
            }
        }

        public string SaldoDoDiaFormatado
        {
            get
            {
                if (SaldoDoDia.HasValue)
                {
                    return (SaldoDoDia.Value < TimeSpan.Zero ? "-" + SaldoDoDia.Value.ToString(@"hh\:mm\:ss") : SaldoDoDia.Value.ToString(@"hh\:mm\:ss"));
                }
                return string.Empty;
            }
        }

        public PontoListarModel ()
        {
            this.Turnos = new List<PontoEntradaSaidaModel>();
        }

        public PontoListarModel(DateTime dia)
            : this()
        {
            this.Dia = dia;
        }

        public void AddRegistroPonto(int idPonto, DateTime? entrada, DateTime? saida, string obs)
        {
            Turnos.Add(new PontoEntradaSaidaModel(idPonto, entrada, saida, obs));
        }
    }

    public class PontoEntradaSaidaModel
    {
        public int PontoId { get; set; }
        public DateTime? Entrada { get; set; }
        public DateTime? Saida { get; set; }
        public string Observacao { get; set; }

        public PontoEntradaSaidaModel(int idPonto, DateTime? entrada, DateTime? saida, string obs)
        {
            this.PontoId = idPonto;
            this.Entrada = entrada;
            this.Saida = saida;
            this.Observacao = obs;
        }
    }
}