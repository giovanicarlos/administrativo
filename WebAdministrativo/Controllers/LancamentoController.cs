﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Repositorio;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class LancamentoController : BasicController
    {
        private LancamentoRepository rpLancamento = new LancamentoRepository();
        private ContaRepository rpConta = new ContaRepository();
        private CentroCustoRepository rpCentroCusto = new CentroCustoRepository();
        private PessoaRepository rpPessoa = new PessoaRepository();
        private ChequeRepository rpCheque = new ChequeRepository();

        //
        // GET: /Lancamento/

        public ActionResult Index()
        {
            return View(rpLancamento.BuscaTodas());
        }

        public ActionResult Create()
        {
            ViewBag.ContaId = new SelectList(rpConta.BuscaTodas(), "ContaId", "Banco");
            ViewBag.CentrosdeCusto = rpCentroCusto.Todos();
            ViewBag.Clientes = rpPessoa.Todos();
            ViewBag.Cheques = rpCheque.Todos();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Lancamento lancamento)
        {
            rpLancamento.Adiciona(lancamento);
            return RedirectToAction("Index");
        }

    }
}
