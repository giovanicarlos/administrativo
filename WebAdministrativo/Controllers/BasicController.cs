﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Servico;

namespace WebAdministrativo.Controllers
{
    public class BasicController : Controller
    {
        protected PontoServico pontoServico = new PontoServico();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (User.Identity.IsAuthenticated)
               filterContext.Controller.ViewData["NomeAdministrador"] = new WebAdministrativo.Controllers.AdministradorController(Convert.ToInt32(User.Identity.Name)).Consultar().Nome;
        }
    }
}
