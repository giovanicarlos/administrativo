﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Repositorio;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class PessoaController : BasicController
    {
        private PessoaRepository rpPessoa = new PessoaRepository();
        private PessoaFisicaRepository rpPessoaFisica = new PessoaFisicaRepository();
        //private PessoaJuridicaRepository rpPessoaJuridica = new PessoaJuridicaRepository();

        //
        // GET: /Pessoa/

        public ActionResult Index()
        {
            return View(Todos());
        }

        public ActionResult Editar()
        {
            return View();
        }

        public ActionResult Cadastrar(int? id)
        {
            if (id != null)
            {
                Cliente cliente = new ClienteController(Convert.ToInt32(id)).Consultar();
                //PessoaFisica pFisica = rpPessoaFisica.GetById(Convert.ToInt32(id));
                //if (pFisica != null)
                //    cliente = new Cliente(pFisica);
                //else
                //{
                //    PessoaJuridica pJuridica = rpPessoaJuridica.GetById(Convert.ToInt32(id));
                //    if (pJuridica != null)
                //        cliente = new Cliente(pJuridica);
                //}
                //Pessoa pessoa = rpPessoa.GetById(Convert.ToInt32(id));
                //cliente.Nome = pessoa.Nome;
                return View(cliente);
            }
            else
                return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(Cliente cliente)
        {
            if (cliente.enmTipoPessoa == enumTipoPessoa.Fisica)
            {

            }

            PessoaFisica pessoa = new PessoaFisica();
            pessoa.Nome = "Nova pessoa";
            rpPessoaFisica.Adiciona(pessoa);
            return RedirectToAction("Index");
        }

        //[HttpPost]
        //public ActionResult Novo(Cliente _cliente)
        //{
        //    //rpPessoa.Adiciona(_cliente);
        //    return RedirectToAction("Index");
        //}

        #region Dominio

        public IEnumerable<Pessoa> Todos()
        {
            return rpPessoa.Todos();
        }

        #endregion

    }
}
