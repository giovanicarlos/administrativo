﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class HomeController : BasicController
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Bem-vindo(a) ao Sistema Administrativo!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
