﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Repositorio;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class CentroCustoController : BasicController
    {
        private CentroCustoRepository rpCentroCusto = new CentroCustoRepository();
        private CentroCusto centroCusto = new CentroCusto();
        public CentroCustoController() { }
        public CentroCustoController(int idCentroCusto)
        {
            centroCusto = rpCentroCusto.GetById(idCentroCusto);
        }


        public ActionResult Index()
        {
            return View(Todos());
        }

        #region Dominio

        public CentroCusto Consultar()
        {
            return centroCusto;
        }

        public IEnumerable<CentroCusto> Todos()
        {
            return rpCentroCusto.Todos();
        }

        #endregion

    }
}
