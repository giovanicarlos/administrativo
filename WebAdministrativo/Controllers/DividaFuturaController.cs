﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Repositorio;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class DividaFuturaController : BasicController
    {
        private DividaFuturaRepository dividaRp = new DividaFuturaRepository();
        //
        // GET: /DividaFutura/

        public ActionResult Index()
        {
            return View(dividaRp.BuscaTodas());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(DividaFutura dividaFutura)
        {
            dividaRp.Adiciona(dividaFutura);
            return RedirectToAction("Index");
        }
    }
}
