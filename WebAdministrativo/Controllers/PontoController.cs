﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Dominio;
using WebAdministrativo.Models;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class PontoController : BasicController
    {
        public ActionResult Index()
        {
            IEnumerable<Ponto> pontos = pontoServico.PontosMesAtual().ToList();
            List<PontoListarModel> pontosDoMes = new List<PontoListarModel>();

            foreach (Ponto ponto in pontos)
            {
                PontoListarModel novoPonto;
                novoPonto = pontosDoMes.Where(x => x.Dia.Date == ponto.Dia.Date).FirstOrDefault();
                if (novoPonto != null)
                {
                    novoPonto.AddRegistroPonto(ponto.PontoId, ponto.Entrada, ponto.Saida, ponto.Observacao);
                }
                else
                {
                    novoPonto = new PontoListarModel(ponto.Dia);
                    pontosDoMes.Add(novoPonto);
                    novoPonto.AddRegistroPonto(ponto.PontoId, ponto.Entrada, ponto.Saida, ponto.Observacao);
                }
            }
            return View(pontosDoMes);
        }

        public ActionResult Novo()
        {
            return View("Editar");
        }

        [HttpPost]
        public ActionResult Novo(PontoEditarModel model)
        {
            DateTime? entrada = null;
            DateTime? saida = null;
            if (model.Entrada.HasValue)
                entrada = new DateTime(model.Dia.Year, model.Dia.Month, model.Dia.Day, model.Entrada.Value.Hour, model.Entrada.Value.Minute, model.Entrada.Value.Second);

            if (model.Saida.HasValue)
            {
                if (model.Entrada.HasValue && model.Entrada.Value.Hour > model.Saida.Value.Hour)
                    saida = new DateTime(model.Dia.Year, model.Dia.Month, model.Dia.Day + 1, model.Saida.Value.Hour, model.Saida.Value.Minute, model.Saida.Value.Second);
                else
                    saida = new DateTime(model.Dia.Year, model.Dia.Month, model.Dia.Day, model.Saida.Value.Hour, model.Saida.Value.Minute, model.Saida.Value.Second);
            }

            if (model.PontoId.HasValue)
            {
                Ponto ponto = pontoServico.GetById(model.PontoId.Value);
                if (ponto != null)
                {
                    ponto.Alterar(model.Dia, entrada, saida, model.Observacao);
                    pontoServico.Update(ponto);
                }
            }
            else
            {
                pontoServico.Add(new Ponto(model.Dia, entrada, saida, model.Observacao));
            }
            return RedirectToAction("Index");
        }

        public ActionResult Editar(int id)
        {
            Ponto ponto = pontoServico.GetById(id);
            if (ponto != null)
            {
                PontoEditarModel model = new PontoEditarModel(ponto.PontoId, ponto.Dia, ponto.Entrada, ponto.Saida, ponto.Observacao, ponto.DataCadastro);
                return View("Editar", model);
            }
            return View("Editar");
        }

        public ActionResult Delete(int id)
        {
            Ponto ponto = pontoServico.GetById(id);
            if (ponto != null)
            {
                pontoServico.Delete(ponto);
            }
            return RedirectToAction("Index");
        }

    }
}
