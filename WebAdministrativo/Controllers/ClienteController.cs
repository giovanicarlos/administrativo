﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Dominio;
using WebAdministrativo.Repositorio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ClienteController : Controller
    {
        private PessoaFisicaRepository rpPessoaFisica = new PessoaFisicaRepository();
        private PessoaJuridicaRepository rpPessoaJuridica = new PessoaJuridicaRepository();
        private Cliente cliente;
        public ClienteController() { }
        public ClienteController(int idCliente)
        {
            PessoaFisica pFisica = rpPessoaFisica.GetById(idCliente);
            if (pFisica != null)
                cliente = new Cliente(pFisica);
            else
            {
                PessoaJuridica pJuridica = rpPessoaJuridica.GetById(idCliente);
                if (pJuridica != null)
                    cliente = new Cliente(pJuridica);
            }
        }

        #region Dominio

        public Cliente Consultar()
        {
            return cliente;
        }

        #endregion

    }
}
