﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Repositorio;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ChequeController : BasicController
    {
        private ChequeRepository rpCheque = new ChequeRepository();
        private Cheque cheque = new Cheque();
        //public ChequeController() { }
        //public ChequeController(int idCheque)
        //{
        //    cheque = rpCheque.GetById(idCheque);
        //}

        public ActionResult Index()
        {
            return View(Todos());
        }

        public ActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Novo(Cheque _cheque)
        {
            rpCheque.Adiciona(_cheque);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Cheque cheque = rpCheque.GetById(id);
            return View(cheque);
        }

        [HttpPost]
        public ActionResult Edit(Cheque _cheque)
        {
            rpCheque.Altera(_cheque);
            return RedirectToAction("Index");
        }

        #region Dominio

        public Cheque Consultar()
        {
            return cheque;
        }

        public IEnumerable<Cheque> Todos()
        {
            return rpCheque.Todos();
        }

        #endregion

    }
}
