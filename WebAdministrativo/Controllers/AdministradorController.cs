﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAdministrativo.Repositorio;
using WebAdministrativo.Dominio;

namespace WebAdministrativo.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class AdministradorController : BasicController
    {
        private AdministradorRepository rpAdministrador = new AdministradorRepository();
        private Administrador administrador = new Administrador();
        public AdministradorController() { }
        public AdministradorController(int idPessoa)
        {
            administrador = rpAdministrador.GetById(idPessoa);
        }

        public ActionResult Index()
        {
            return View();
        }

        #region Dominio

        public Administrador Consultar()
        {
            return administrador;
        }

        public bool AlterarSenha(string senhaAtiga, string novaSenha)
        {
            if (administrador != null)
            {
                administrador.Senha = novaSenha.ToCriptografa();
                rpAdministrador.Update(administrador);
                rpAdministrador.Commit();
                return true;
            }
            return false;
        }

        #endregion
    }
}
