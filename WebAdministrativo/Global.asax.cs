﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using System.Data.Entity;

namespace WebAdministrativo
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "EditarCliente", // Route name
                "cliente/editar/{id}", // URL with parameters
                new { controller = "Pessoa", action = "Cadastrar", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "CadastrarCliente", // Route name
                "cliente/novo", // URL with parameters
                new { controller = "Pessoa", action = "Cadastrar" } // Parameter defaults
            );

            routes.MapRoute(
                "Clientes", // Route name
                "cliente/{action}", // URL with parameters
                new { controller = "Pessoa", action = "Index" } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<WebAdministrativo.Repositorio.AdministrativoContext, WebAdministrativo.Migrations.Configuration>());

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}